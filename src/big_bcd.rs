use bcd::Bcd;
use bcd_type::BcdType;
use std::fmt;
use std::iter::{repeat, FromIterator};
use std::marker::PhantomData;
use std::ops::AddAssign;

use storage::Storage;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct BigBcd<S, D>
where
    S: Storage<D>,
    D: BcdType,
{
    storage: S,
    phantom: PhantomData<D>,
}

impl<'a, S, D> AddAssign<&'a Self> for BigBcd<S, D>
where
    S: Storage<D>,
    D: BcdType,
{
    fn add_assign(&mut self, other: &'a Self) {
        while self.storage.as_ref().len() < other.storage.as_ref().len() {
            self.storage.append(D::from(0)).unwrap();
        }

        let mut overflow = false;
        for (a, b) in self
            .storage
            .as_mut()
            .iter_mut()
            .zip(other.storage.as_ref().iter().chain(repeat(&D::from(0))))
        {
            let (r, o) = Bcd(*a).add_with_overflow(Bcd(*b), overflow);
            *a = r.0;
            overflow = o;
        }

        if overflow {
            self.storage.append(D::from(1)).unwrap();
        }
    }
}

impl<S, D> BigBcd<S, D>
where
    S: Storage<D>,
    D: BcdType,
{
    pub fn iter<'a>(&'a self) -> BigBcdDigitIter<'a, D> {
        BigBcdDigitIter {
            data: &self.storage.as_ref(),
            index: self.storage.as_ref().len() * Bcd::<D>::digits(),
        }
    }

    pub fn from_storage(storage: S) -> Self {
        BigBcd {
            storage,
            phantom: PhantomData,
        }
    }
}

impl<S, D> From<Vec<D>> for BigBcd<S, D>
where
    S: Storage<D> + FromIterator<D>,
    D: BcdType,
{
    fn from(v: Vec<D>) -> Self {
        BigBcd {
            storage: v.iter().cloned().collect(),
            phantom: PhantomData,
        }
    }
}

impl<S, D> Into<Vec<D>> for BigBcd<S, D>
where
    S: Storage<D>,
    D: BcdType,
{
    fn into(self) -> Vec<D> {
        self.storage.as_ref().iter().cloned().collect()
    }
}

impl<'a, S, D> From<&'a str> for BigBcd<S, D>
where
    S: Storage<D> + FromIterator<D>,
    D: BcdType,
{
    fn from(s: &'a str) -> Self {
        let n = Bcd::<D>::digits() - 1;
        let l = s.len();
        let chunks = (l + n - 1) / n;
        let iter = (0..chunks)
            .map(|i| &s[l - (n * (i + 1)).min(l)..l - n * i])
            .map(|s| Bcd::<D>::from(s).0);
        BigBcd {
            storage: S::from_iter(iter),
            phantom: PhantomData,
        }
    }
}

impl<S, D> fmt::Display for BigBcd<S, D>
where
    S: Storage<D> + FromIterator<D>,
    D: BcdType,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for d in self.iter().skip_while(|d| *d == 0) {
            write!(f, "{}", d)?;
        }
        Ok(())
    }
}

pub struct BigBcdDigitIter<'a, D: BcdType + 'a> {
    data: &'a [D],
    index: usize,
}

impl<'a, D: BcdType + 'a> Iterator for BigBcdDigitIter<'a, D> {
    type Item = u8;

    fn next(&mut self) -> Option<u8> {
        let subtract = match self.index % Bcd::<D>::digits() {
            0 => 2,
            _ => 1,
        };
        self.index = self
            .index
            .checked_sub(subtract)
            .unwrap_or(usize::max_value());
        self.data
            .get(self.index / Bcd::<D>::digits())
            .map(|d| Bcd(*d).digit((self.index % Bcd::<D>::digits()) as u8))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn correct_digits() {
        assert_eq!(
            BigBcd::from_storage(vec![0x01234467u32, 0x01235567])
                .iter()
                .collect::<Vec<_>>(),
            vec![1, 2, 3, 5, 5, 6, 7, 1, 2, 3, 4, 4, 6, 7]
        );
    }

    #[test]
    fn add_assign() {
        let mut n = BigBcd::<Vec<u32>, _>::from("123456789012");
        let m = n.clone();
        n += &m;
        assert_eq!(&n.to_string(), "246913578024");
    }

    #[test]
    fn from_str() {
        assert_eq!(
            BigBcd::<Vec<u32>, _>::from("1234"),
            BigBcd::<Vec<u32>, _>::from(vec![0x1234])
        );
        assert_eq!(
            BigBcd::<Vec<u32>, _>::from("12345678"),
            BigBcd::<Vec<u32>, _>::from(vec![0x2345678, 0x1])
        );
        assert_eq!(
            BigBcd::<Vec<u64>, _>::from("1304969544928657"),
            BigBcd::<Vec<u64>, _>::from(vec![0x304969544928657, 0x1])
        );
    }
}
