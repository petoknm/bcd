use std::mem::size_of;
use std::ops::{BitOrAssign, ShlAssign};

pub trait BcdMasks: Sized + From<u8> + ShlAssign + BitOrAssign {

    /// 0x0666
    fn sixes() -> Self {
        let nibbles = 2 * size_of::<Self>() as u8;
        let mut n = 0.into();
        for i in 0..nibbles - 1 {
            let mut six: Self = 0x6.into();
            six <<= (4 * i).into();
            n |= six;
        }
        n
    }

    /// 0x1110
    fn ones() -> Self {
        let nibbles = 2 * size_of::<Self>() as u8;
        let mut n = 0.into();
        for i in 1..nibbles {
            let mut one: Self = 0x1.into();
            one <<= (4 * i).into();
            n |= one;
        }
        n
    }

    /// 0xF000
    fn top_bcd() -> Self {
        let mut n = 0xF.into();
        let bytes = size_of::<Self>() as u8;
        n <<= (8 * bytes - 4).into();
        n
    }
}

impl BcdMasks for u8 {}
impl BcdMasks for u16 {}
impl BcdMasks for u32 {}
impl BcdMasks for u64 {}
impl BcdMasks for u128 {}
