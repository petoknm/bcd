use std::mem::size_of;
use std::ops::Add;

use bcd_type::BcdType;

#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub struct Bcd<T: BcdType>(pub T);

impl<T: BcdType> Bcd<T> {
    #[inline]
    pub const fn digits() -> usize {
        size_of::<T>() * 2
    }

    #[inline]
    pub fn digit(self, n: u8) -> u8 {
        let shift = (4 * n).into();
        ((self.0 >> shift) & 0xF.into()).to_u8().unwrap()
    }

    #[inline]
    pub fn add_with_overflow(self, other: Self, overflow: bool) -> (Self, bool) {
        let (r1, o1) = self + other;
        let (r2, o2) = r1 + Bcd((if overflow { 1 } else { 0 }).into());
        (r2, o1 | o2)
    }
}

impl<T: BcdType> Add for Bcd<T> {
    type Output = (Self, bool);

    /// http://homepage.cs.uiowa.edu/~jones/bcd/bcd.html#packed
    #[inline]
    fn add(self, other: Self) -> Self::Output {
        if (self.0 & T::top_bcd() != 0.into()) || (other.0 & T::top_bcd() != 0.into()) {
            panic!("Addition will overflow");
        }
        let t1 = self.0 + T::sixes();
        let t2 = t1 + other.0;
        let t3 = t1 ^ other.0;
        let t4 = t2 ^ t3;
        let t5 = (!t4) & T::ones();
        let t6 = (t5 >> 2.into()) | (t5 >> 3.into());

        let t7 = t2 - t6;
        (Bcd(t7 & (!T::top_bcd())), t7 & T::top_bcd() != 0.into())
    }
}

impl<'a, T: BcdType> From<&'a str> for Bcd<T> {
    fn from(s: &'a str) -> Self {
        let mut v = T::from(0);
        for (i, c) in s.chars().rev().enumerate() {
            v = v | (T::from(c.to_digit(10).unwrap() as u8) << T::from(4 * i as u8));
        }
        Bcd(v)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn correct_digits_u32() {
        let digits: Vec<_> = (0..Bcd::<u32>::digits())
            .map(|i| Bcd(0x01234567u32).digit(i as u8))
            .collect();

        assert_eq!(digits, vec![7, 6, 5, 4, 3, 2, 1, 0]);
    }

    #[test]
    fn correct_digits_u64() {
        let digits: Vec<_> = (0..Bcd::<u64>::digits())
            .map(|i| Bcd(0x0123456789012345u64).digit(i as u8))
            .collect();

        assert_eq!(digits, vec![5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);
    }

    #[test]
    fn add_bcd_u32() {
        assert_eq!(
            Bcd(0x01234567u32) + Bcd(0x01234567),
            (Bcd(0x02469134), false)
        );
        assert_eq!(
            Bcd(0x05000000u32) + Bcd(0x05000000),
            (Bcd(0x00000000), true)
        );
        assert_eq!(
            Bcd(0x09000000u32) + Bcd(0x09000000),
            (Bcd(0x08000000), true)
        );
    }

    #[test]
    fn from_str() {
        assert_eq!(Bcd::<u32>::from("10"), Bcd(0x10u32));
    }
}
