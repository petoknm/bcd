pub struct BinaryFibIter(usize, usize);

impl BinaryFibIter {
    pub fn new() -> BinaryFibIter {
        BinaryFibIter(0, 1)
    }
}

impl Iterator for BinaryFibIter {
    type Item = usize;
    fn next(&mut self) -> Option<usize> {
        *self = BinaryFibIter(self.1, self.0 + self.1);
        Some(self.1)
    }
}
