type Digit = u64;
use bcd::BigBcd;
use std::mem::swap;

pub type Fib = BigBcd<Vec<Digit>, Digit>;
pub struct InfiniteBcdFibIter(Fib, Fib);

impl InfiniteBcdFibIter {
    pub fn new() -> InfiniteBcdFibIter {
        InfiniteBcdFibIter(vec![0].into(), vec![1].into())
    }

    pub fn advance(&mut self) {
        self.0 += &self.1;
        swap(&mut self.0, &mut self.1);
    }

    pub fn get<'a>(&'a self) -> &'a Fib {
        &self.1
    }
}

impl Iterator for InfiniteBcdFibIter {
    type Item = Fib;

    fn next(&mut self) -> Option<Self::Item> {
        self.advance();
        Some(self.get().clone())
    }
}
