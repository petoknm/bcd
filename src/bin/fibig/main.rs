#![feature(test)]
extern crate bcd;
extern crate generic_array;
extern crate test;
// extern crate mongodb;
// extern crate r2d2;
// extern crate r2d2_mongodb;
// extern crate rayon;

// use mongodb::gridfs::{Store, ThreadedStore};
// use r2d2_mongodb::MongodbConnectionManager;
// use std::io::Write;

mod binary_fib_iter;
use binary_fib_iter::*;

mod big_bcd_fib_iter;
use big_bcd_fib_iter::*;

mod infinite_bcd_fib_iter;
use infinite_bcd_fib_iter::*;

fn main() {
    for fib in BinaryFibIter::new() {
        println!("{}", fib);
    }

    for fib in BcdFibIter::new() {
        println!("{}", fib);
    }

    for fib in InfiniteBcdFibIter::new() {
        println!("{}", fib);
    }

    // let manager = MongodbConnectionManagerBuilder::new()
    //     .with_host("localhost")
    //     .with_port(27017)
    //     .with_db("admin")
    //     .with_username("root")
    //     .with_password("password")
    //     .build();
    //
    // let pool = r2d2::Pool::builder().max_size(64).build(manager).unwrap();
    //
    // let thread_pool = rayon::ThreadPoolBuilder::new()
    //     .num_threads(64)
    //     .build()
    //     .unwrap();
    //
    // for (i, fib) in BcdFibIter::new().enumerate() {
    //     println!("Calculated {}th fibonacci", i);
    //     let pool = pool.clone();
    //     thread_pool.spawn(move || {
    //         let db = pool.get().unwrap();
    //         let store = Store::with_db(db.clone());
    //         let mut file = store.create(format!("{}.txt", i)).unwrap();
    //         write!(file, "{}", fib).unwrap();
    //         println!("Stored {}th fibonacci", i);
    //     });
    //     std::thread::sleep(std::time::Duration::from_millis(3));
    // }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn binary_equals_bcd() {
        let n = 90;
        let binary = BinaryFibIter::new().map(|n| n.to_string());
        let bcd = BcdFibIter::new().map(|n| n.to_string());
        let incorrect: Vec<_> = binary
            .zip(bcd)
            .take(n)
            .filter(|(a, b)| {
                println!("{} ?= {}", a, b);
                a != b
            })
            .collect();
        assert_eq!(incorrect, vec![]);
    }
}
