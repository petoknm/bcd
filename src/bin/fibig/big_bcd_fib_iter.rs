use bcd::BigBcd;
use std::mem::swap;

use generic_array::typenum::U1024;
use generic_array::GenericArray;

type Digit = u64;
type Length = U1024;

type Storage = GenericArray<Digit, Length>;
pub type Fib = BigBcd<Storage, Digit>;
pub struct BcdFibIter(Fib, Fib);

impl BcdFibIter {
    pub fn new() -> BcdFibIter {
        let a = Storage::default();
        let mut b = a.clone();
        b[0] = 1;
        BcdFibIter(BigBcd::from_storage(a), BigBcd::from_storage(b))
    }

    pub fn advance(&mut self) {
        self.0 += &self.1;
        swap(&mut self.0, &mut self.1);
    }

    pub fn get<'a>(&'a self) -> &'a Fib {
        &self.1
    }
}

impl Iterator for BcdFibIter {
    type Item = Fib;

    fn next(&mut self) -> Option<Self::Item> {
        self.advance();
        Some(self.get().clone())
    }
}
