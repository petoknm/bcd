use std::ops::{Add, BitAnd, BitOr, BitXor, Not, Shl, Shr, Sub};
use num::traits::ToPrimitive;

use bcd_masks::BcdMasks;

pub trait BcdType:
    BcdMasks
    + Add<Output = Self>
    + BitAnd<Output = Self>
    + BitOr<Output = Self>
    + BitXor<Output = Self>
    + Not<Output = Self>
    + Sub<Output = Self>
    + Shr<Output = Self>
    + Shl<Output = Self>
    + From<u8>
    + ToPrimitive
    + PartialEq
    + Eq
    + Copy
{
}

impl BcdType for u8 {}
impl BcdType for u16 {}
impl BcdType for u32 {}
impl BcdType for u64 {}
impl BcdType for u128 {}
