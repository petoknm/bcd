use generic_array::{ArrayLength, GenericArray};

pub trait Storage<T>: AsRef<[T]> + AsMut<[T]> {
    fn append(&mut self, T) -> Result<(), ()> {
        Err(())
    }
}

impl<T> Storage<T> for Vec<T> {
    fn append(&mut self, t: T) -> Result<(), ()> {
        self.push(t);
        Ok(())
    }
}

impl<T, N> Storage<T> for GenericArray<T, N> where N: ArrayLength<T> {}
