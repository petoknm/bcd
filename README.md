# bcd

Library for binary coded decimal numbers

## Documentation

Coming soon...

## Example

```rust
assert_eq!(
    Bcd(0x01234) +
    Bcd(0x01234),
   (Bcd(0x02468), /* overflow */ false)
);
```
